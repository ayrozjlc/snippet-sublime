# Configuración Sublime Text 3

##### 1.- Ir a la siguiente carpeta(desde terminal):
```
// Validado en Debian y Ubuntu
cd $HOME/.config/sublime-text3/Packages/User
```

##### 2.- Clonar el repositorio en la carpeta(desde terminal):
```
git clone https://gitlab.com/ayrozjlc/snippet-sublime.git
```

##### 3.- Iniciar Sublime Text:
[Instalar Package Control](https://packagecontrol.io/installation)

##### 4.-  Instalar Material Theme y Monokai Rich(desde Sublime Text):
```
Ctrl + Shift + p, e ingresar la palabra:  install Package(enter)
buscar: Material Theme y después Monokai Rich
```

##### 5.- Ejecutar el siguiente comando(desde terminal):
```
//Ir a carpeta clonada
cd snippet-sublime
// Dar permiso de ejecución chmod 755 z_install.sh
./z_install.sh
```

##### 6.- Instalar los paquetes adicionales(desde sublime):
```
// Ctrl + Shift + p, e ingresar la palabra:  install Package(enter)
- A File Icon
- All Autocomplete
- Angular 2 Snippets
- Angular CLI
- bootstrap 4x Autocomplete
- bootstrap 4x Snippets
- BracketHighlighter
- Color Highlighter
- CompletePHP
- DocBlockr
- Emmet
- Focus File on Sidebar
- GitGutter
- HTML Snippets
- HTML5
- JavaScript Completions
- javaScript Snippets
- jQuery
- jQuery Snippets pack
- Markdown Preview
- Material Theme
- Monokai Rich
- Ngx HTML
- Origami
- Phpcs(phpcs.sublime-settings contiene configuración básica para php-cs-fixer)
- SideBarEnhancements
- Symfony2 Snippets
- TypeScript
- Yii2 Auto-complete
- Yii2 Snippets
```
***

#### Ver snippets disponibles con esta instalación:
> Ctrl + Shift + p, e ingresar la palabra: ayroz:

***
> Documentación Adicional:
- [Atajos de teclado](http://falasco.org/sublime-text-2-cheat-sheet)
- [Manual EMMET](http://docs.emmet.io/cheat-sheet/)
- [Información a detalle de cada paquete](https://packagecontrol.io/browse)
- [Detalle de paquete phpcs](https://benmatselby.dev/sublime-phpcs/)
