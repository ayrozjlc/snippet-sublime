#!/bin/bash
options='Preferences-Sublime lampp_aliases Help Quit'

copy_preferences() {
	dir=$(pwd)
	preferences=$(find Preferences.sublime-settings)

	if [ $preferences = "Preferences.sublime-settings" ]
	then
		cp "$dir/$preferences" ${dir/${PWD##*/}/$preferences}
		echo -e "\n\e[32mSe copio el archivo correctamente.\e[0m \n"
	else
		echo -e "\n\e[31mOcurrio un error(Asegurese de estar en el directorio de la carpeta snippet-sublime).\e[0m \n"
	fi
}

copy_lamppAliases() {
	file=$HOME/.bash_aliases
	alias=$(cat "$(pwd)/z_xampp_aliases.txt")
	echo $alias >> $file
	echo -e "\n\e[32mSe agregaron los alias correctamente.\e[0m \n"
}

Help() {
	echo
	echo -e "\e[31mPreferences-Sublime\e[0m       Se copia archivo de preferencias a ruta Sublime Text."
	echo -e "\e[31mlampp_aliases\e[0m             Alias para LAMPP(ver z_xampp_aliases.txt para mas detalle)."
	echo -e "\e[31mHelp\e[0m                      Lista y descripción de opciones de instalador."
	echo -e "\e[31mQuit\e[0m                      Terminar ejecutable."
	echo
}

echo Seleccionar una opción:
echo =======================

select name in $options
do
if [ $name == 'Quit' ]
then
break
elif [ $name == 'Preferences-Sublime' ]
then
	copy_preferences
elif [ $name == 'lampp_aliases' ]
then
	copy_lamppAliases
elif [ $name == 'Help' ]
then
	Help
else
	echo -e "\n \e[31mOpción no valida.\e[0m \n"
fi
done
echo -e "\n \e[34mBye\e[0m \n"